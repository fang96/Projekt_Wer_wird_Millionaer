################################

Das Team besteht aus: Baron, Sebastian und Angirillo, Fabio

Baron Sebastian ist für die Fragen zuständig und packt jede Frage in eine eigenständige Funktion. Eine Frage wird dann per Zufall ausgewählt.

Angirillo Fabio ist für die Basis des Spiels verantwortlich. Er wird den Zufallsgenerator für die Fragen, die Eingabe und Ausgabe des Programms und den Geldstand erstellen.


Einzelne Feinschliffe werden im letzten Entwicklungsschritt von beiden Teammitglieder durchgeführt.