// wer wird millionaer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include "fragen.h"
#include <time.h>
#include <conio.h>

int i;
int k;
int fz;
int p1, p2, p3, p4;
int jokt;
int antwort;
char antweingabe;
int korrekt;
int jokcount;
int jokeruse;
int jokerusek;
int jokerft;
int helptpjok;
int helpttjok;
int breaker;
int naptime = 10;
char jokcat[4][17] = { "  (1)  50/50   |" , "  (2) Telefon   " , "|  (3)  Publikum" ,"                " };
char *jp1, *jp2, *jp3;
char entscheid;
char balken = 219;
int repeat;
int end;
int gewinn[15] = { 50, 100, 200, 300, 500, 1000, 2000, 4000, 8000, 16000, 32000, 64000, 125000, 500000, 1000000 };
int jokcap1, jokcap2, jokcap3;

void trenner() {		// Trennstrich
	printf("_________________________________________________________________________________\n\n");
}

void titel() {	// Titelzeile
	trenner();
	printf("#\t\t\t\t\t\t\t\t\t\t#\n#\t\t\t\t\t\t\t\t\t\t#\n#\t\t\t\tWer wird Million\x84r\t\t\t\t#\n#\t\t\t\t\t\t\t\t\t\t#\n#\t\t\t\t\t\t\t\t\t\t#\n");
	trenner();
}

void reset() { // Zuruecksetzen der Variablen, falls direkt nochmal gespielt wird
	jp1 = jokcat[0];
	jp2 = jokcat[1];
	jp3 = jokcat[2];
	jokcap1 = 49;
	jokcap2 = 50;
	jokcap3 = 51;
	jokcount = 3;
	repeat = 0;
	entscheid = 2;
	jokeruse = 0;
	jokerusek = 0;
	jokerft = 0;
	helptpjok = 0;
	helpttjok = 0;
}

int willkommen() {		// Anzeige des Startmenüs
	char start;
	system("cls");
	titel();
	printf("\n\n\t\t\tWillkommen bei 'Wer wird Million\x84r'\n\n\n\tBeantworten sie alle 15 Fragen und holen sie holen sich die Million!\n\n\n\n");
	trenner();
	printf("\n\n\n\t\t\t     Spiel starten (Enter)\n\n\n\t\t\t     Doch nicht spielen (Esc)\n\n\n\n\n");
	trenner();
	start = _getch();		// Eingabe um das Spiel zu starten oder zu beenden
	if (start == 13) {
		end = 0;
	}
	else if (start == 27) {
		end = 1;
	}
	else {
		end = 2;
	}
	return end;				// Rückgabe für Spiel starten (starten, beenden, Fehleingabe)
}

int zufallszahl(int i) {	// Ausgabe der Zufallszahlen - gesondert, damit sie waehrend die Joker fuer eine Frage laufen gleich bleiben
	k = rand() % 6;
	fz = rand() % 2;
	jokt = rand() % (100 - (i * 2));
	p1 = rand() % 25;
	p2 = rand() % 25;
	p3 = rand() % 25;
	p4 = rand() % 25;
	return k;
	return fz;
	return jokt;
	return p1, p2, p3, p4;
}

void jokerpubtxt() {	// Text der ablaeuft, wenn der Publikumsjoker gewaehlt wurde
	int i;
	trenner();
	Sleep(300);
	printf("  Warten wir kurz w\x84hrend das Publikum abstimmt.\n\n");
	for (i = 0; i < 15; i++) {
		printf("  .");
		Sleep(200);
	}
	printf("\n\n  Das Publikum hat abgestimmt!\n\n");
	Sleep(1200);
	printf("  Hier sind die Ergebnisse.\n\n");
	Sleep(800);
}

void jokerteltxt() {				// Text der ablaeuft, wenn der Telefonjoker gewaehlt wurde
	trenner();
	Sleep(800);
	printf("  Ring Ring!");
	Sleep(800);
	printf("  Ring Ring!\n\n\n");
	Sleep(1200);
	printf("  Joker:  Ja, Hallo?\n\n");
	Sleep(1200);
	printf("  Sie:\t  Gr\x81\xE1 dich, ich hab hier ne Frage und zwar\n\t  '%s'.\n\n", que[i].fn[k].frage);
	Sleep(2200);
	if (jokeruse != 0) {					// Antworttext. Wird ausgegeben, wenn der 50/50 Joker in der Runde schon gewaehlt wurde
		printf("  Sie:");
		if (jokerusek == 1 || jokeruse == 1) { // Variablen die von joker50() kommen, geben an welche Antworten beim 50/50 Joker gewaehlt wurden.
			printf("\t  'A: %s' \n", que[i].fn[k].antw1);
		}
		if (jokerusek == 2 || jokeruse == 2) {
			printf("\t'  B: %s' \n", que[i].fn[k].antw2);
		}
		if (jokerusek == 3 || jokeruse == 3) {
			printf("\t  'C: %s' \n", que[i].fn[k].antw3);
		}
		if (jokerusek == 4 || jokeruse == 4) {
			printf("\t  'D: %s' \n", que[i].fn[k].antw4);
		}
	}
	else {							// Antworttext. Wird ausgegeben, wenn der 50/50 Joker in der Runde noch nicht gewaehlt wurde
		printf("  Sie:\t  'A: %s' \n \t  'B: %s' \n \t  'C: %s' \n \t  'D: %s' \n", que[i].fn[k].antw1, que[i].fn[k].antw2, que[i].fn[k].antw3, que[i].fn[k].antw4);
	}
	printf("\n");
	Sleep(1200);
	printf("  Joker:  Da muss ich kurz ueberlegen...\n\n");
	Sleep(2200);
	if (jokeruse == 0) {			// Antwort des Telefonjokers. Wird ausgegeben, wenn der 50/50 Joker in der Runde noch nicht gewaehlt wurde
		if (jokt > 24) {			// Holt sich eine Zahl von 0 bis 100 aus zufallszahl(), wird mit jeder Runde um i*2 kleiner (Runde 15 - Zahl zwischen 0 und 70)
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antwk);
		}
		else if (jokt >= 0 || jokt < 6) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antw1);
		}
		else if (jokt > 5 || jokt < 12) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antw2);
		}
		else if (jokt > 11 || jokt < 18) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antw3);
		}
		else if (jokt > 17 || jokt <= 24) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antw3);
		}
	}
	if (jokeruse == 1) {			// Antwort des Telefonjokers. Wird ausgegeben, wenn der 50/50 Joker in der Runde schon gewaehlt wurde. Fuer den Fall das die zweite Antwort des 50/50 Jokers die 1. Antwort ist.
		if (jokt > 14) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antwk);
		}
		else if (jokt < 15) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antw1);
		}
	}
	if (jokeruse == 2) {			// Antwort des Telefonjokers. Wird ausgegeben, wenn der 50/50 Joker in der Runde schon gewaehlt wurde. Fuer den Fall das die zweite Antwort des 50/50 Jokers die 2. Antwort ist.
		if (jokt > 14) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antwk);
		}
		else if (jokt < 15) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antw2);
		}
	}
	if (jokeruse == 3) {			// Antwort des Telefonjokers. Wird ausgegeben, wenn der 50/50 Joker in der Runde schon gewaehlt wurde. Fuer den Fall das die zweite Antwort des 50/50 Jokers die 3. Antwort ist.
		if (jokt > 14) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antwk);
		}
		else if (jokt < 15) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antw3);
		}
	}
	if (jokeruse == 4) {			// Antwort des Telefonjokers. Wird ausgegeben, wenn der 50/50 Joker in der Runde schon gewaehlt wurde. Fuer den Fall das die zweite Antwort des 50/50 Jokers die 3. Antwort ist.
		if (jokt > 14) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antwk);
		}
		else if (jokt < 15) {
			printf("  Joker:  Ich denke du solltest '%s' nehmen.\n\n", que[i].fn[k].antw4);
		}
	}
	Sleep(800);
	printf("  Sie:\t  Alles klar, dank dir.\n");
	Sleep(800);
}

int joker50(int i, int k, int fz) {			// Antwortext fuer 50/50 Joker.
	if (korrekt == 1) {						// Wird ausgegeben falls die 1. Antwort korrekt ist.
		printf("\tA: %s\n\n", que[i].fn[k].antw1);
		jokerusek = 1;						// Gibt an ,dass der 50/50 Joker die 1. Antwort als Korrekt ausgibt
		if (fz == 0) {						// Holt sich eine Zahl zwischen 0 und 2 aus zufallszahl(). Waehlt die 2. Antwort zu der Korrekten
			printf("\tB: %s\n\n\n\n\n\n", que[i].fn[k].antw2);
			jokeruse = 2;					// Gibt an, dass der 50/50 Joker die 2. Antwort als Zusatz zur Korrekten Antwort ausgibt
		}
		else if (fz == 1) {
			printf("\n\n\tC: %s\n\n\n\n", que[i].fn[k].antw3);
			jokeruse = 3;
		}
		else if (fz == 2) {
			printf("\n\n\n\n\tD: %s\n\n", que[i].fn[k].antw4);
			jokeruse = 4;
		}
	}
	else if (korrekt == 2) {
		if (fz == 0) {
			printf("\tA: %s", que[i].fn[k].antw1);
			jokeruse = 1;
		}
		printf("\n\n\tB: %s\n\n", que[i].fn[k].antw2);
		jokerusek = 2;
		if (fz == 1) {
			printf("\tC: %s\n\n", que[i].fn[k].antw3);
			jokeruse = 3;
		}
		else if (fz == 2) {
			printf("\n\n\tD: %s", que[i].fn[k].antw4);
			jokeruse = 4;
		}
		else if (fz == 0) {
			printf("\n\n");
		}
		printf("\n\n");
	}
	else if (korrekt == 3) {
		if (fz == 0) {
			printf("\tA: %s\n\n", que[i].fn[k].antw1);
			jokeruse = 1;
		}
		else if (fz == 1) {
			printf("\n\n\tB: %s", que[i].fn[k].antw2);
			jokeruse = 2;
		}
		printf("\n\n\tC: %s\n\n", que[i].fn[k].antw3);
		jokerusek = 3;
		if (fz == 2) {
			printf("\tD: %s", que[i].fn[k].antw4);
			jokeruse = 4;
		}
		printf("\n\n");
	}
	else if (korrekt == 4) {
		if (fz == 0) {
			printf("\tA: %s\n\n\n\n\n\n", que[i].fn[k].antw1);
			jokeruse = 1;
		}
		else if (fz == 1) {
			printf("\n\n\tB: %s\n\n\n\n", que[i].fn[k].antw2);
			jokeruse = 2;
		}
		else if (fz == 2) {
			printf("\n\n\n\n\tC: %s\n\n", que[i].fn[k].antw3);
			jokeruse = 3;
		}
		printf("\tD: %s\n\n", que[i].fn[k].antw4);
		jokerusek = 4;
	}
	return jokeruse; // Ausgabe welche Antwort Zusatz ist
	return jokerusek; // Ausgabe welche Antwort die Korrekte ist
}

void jokerpub(int i, int k, int p1, int p2, int p3, int p4) {	// Antworttext für den Publikumsjoker. Ausgabe mit Rauten als Balken fuer die Abstimmung
	int o, p;
	int ak = 45 - i + 2;				// Jede Runde verinngert sich die Anzahl der auszugebenden Blöcke 
	int af = 0 + i * 2;
	if (jokeruse != 0) {				// Antworten, falls der 50/50 Joker schon gewaehlt wurde
		if (korrekt == 1) {				// Falls die erste Antwort korrekt ist ----
			printf("\tA: %s\n", que[i].fn[k].antw1);
			printf("\t");
			p = ak + p1;				// ---- werden 45 Blöcke ausgegeben plus 5 und einer Zahl zwischen 0 und 25, die aus zufallszahl() kommt
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
			printf("\n\n");
			if (fz == 0) {				// Falls Zusatzantwort zur korrekten Antwort 1 die 2. Antwort ist
				printf("\tB: %s\n", que[i].fn[k].antw2);
				printf("\t");
				p = af + p2;				// Es werden 0 bis 25 Blöcke ausgegeben + Eine Anzahl die sich jede Runde um i*2 vergrößert
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
				printf("\n\n\n\n\n\n");
			}
			else if (fz == 1) {
				printf("\n\n\tC: %s\n", que[i].fn[k].antw3);
				printf("\t");
				p = af + p3;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
				printf("\n\n\n\n");
			}
			else if (fz == 2) {
				printf("\n\n\n\n\tD: %s\n", que[i].fn[k].antw4);
				printf("\t");
				p = af + p4;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
				printf("\n\n");
			}
		}
		else if (korrekt == 2) {
			if (fz == 0) {
				printf("\tA: %s\n", que[i].fn[k].antw1);
				printf("\t");
				p = af + p1;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
			}
			printf("\n\n\tB: %s\n", que[i].fn[k].antw2);
			printf("\t");
			p = ak + p2;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
			printf("\n\n");
			if (fz == 1) {
				printf("\tC: %s\n", que[i].fn[k].antw3);
				printf("\t");
				p = af + p3;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
				printf("\n\n");
			}
			else if (fz == 2) {
				printf("\n\n\tD: %s\n", que[i].fn[k].antw4);
				printf("\t");
				p = af + p4;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
			}
			else if (fz == 0) {
				printf("\n\n");
			}
			printf("\n\n");
		}
		else if (korrekt == 3) {
			if (fz == 0) {
				printf("\tA: %s\n", que[i].fn[k].antw1);
				printf("\t");
				p = af + p1;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
				printf("\n\n");
			}
			else if (fz == 1) {
				printf("\n\n\tB: %s\n", que[i].fn[k].antw2);
				printf("\t");
				p = af + p2;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
			}
			printf("\n\n\tC: %s\n", que[i].fn[k].antw3);
			printf("\t");
			p = ak + p3;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
			printf("\n\n");
			if (fz == 2) {
				printf("\tD: %s\n", que[i].fn[k].antw4);
				printf("\t");
				p = af + p4;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
			}
			printf("\n\n");
		}
		else if (korrekt == 4) {
			if (fz == 0) {
				printf("\tA: %s\n", que[i].fn[k].antw1);
				printf("\t");
				p = af + p1;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
				printf("\n\n\n\n\n\n");
			}
			else if (fz == 1) {
				printf("\n\n\tB: %s\n", que[i].fn[k].antw2);
				printf("\t");
				p = af + p2;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
				printf("\n\n\n\n");
			}
			else if (fz == 2) {
				printf("\n\n\n\n\tC: %s\n", que[i].fn[k].antw3);
				printf("\t");
				p = af + p3;
				for (o = 0; o < p; o++) {
					printf("%c", balken);
				}
				printf("\n\n");
			}
			printf("\tD: %s\n", que[i].fn[k].antw4);
			printf("\t");
			p = ak + p4;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
			printf("\n\n");
		}
	}
	else { // Antworten für den Publikumsjoker, falls der 50/50 Joker noch nicht gewählt wurde
		printf("\tA: %s\n", que[i].fn[k].antw1);
		printf("\t");
		if (korrekt == 1) {
			p = ak + p1;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
		}
		else {
			p = af + p1;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
		}
		printf("\n\n");
		printf("\tB: %s\n", que[i].fn[k].antw2);
		printf("\t");
		if (korrekt == 2) {
			p = ak + p2;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
		}
		else {
			p = af + p2;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
		}
		printf("\n\n");
		printf("\tC: %s\n", que[i].fn[k].antw3);
		printf("\t");
		if (korrekt == 3) {
			p = ak + p3;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
		}
		else {
			p = af + p3;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
		}
		printf("\n\n");
		printf("\tD: %s\n", que[i].fn[k].antw4);
		printf("\t");
		if (korrekt == 4) {
			p = ak + p4;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
		}
		else {
			p = af + p4;
			for (o = 0; o < p; o++) {
				printf("%c", balken);
			}
		}
		printf("\n\n");
	}
}

void jokertel(int i, int k, int jokt) {		// Antworttext für den Telefonjoker
	if (jokeruse != 0) {					// Falls der 50/50 Joker in der Runde schon gewaehlt wurde
		joker50(i, k, fz);					// Ausgabe wie in joker50()
	}
	else {									// Falls der 50/50 Joker in der Runde noch nicht gewählt wurde
		printf("\tA: %s\n\n\tB: %s\n\n\tC: %s\n\n\tD: %s\n\n", que[i].fn[k].antw1, que[i].fn[k].antw2, que[i].fn[k].antw3, que[i].fn[k].antw4);
	}

}

int joker() {					// Anzeige, falls ein Joker benutzt werden soll
	char jok;
	trenner();
	printf("\n  Joker benutzen: %s   %s   %s\n\n", jp1, jp2, jp3);	 // Anzeige, welche Joker vorhanden sind
	trenner();
	printf("  Welchen Joker wollen sie nutzen? ");
	//scanf_s("%c", &jok);		// Eingabe für die Wahl des Jokers
	//getchar();
	jok = _getch();
	if (jok == jokcap1) {		// Ueberprufung ob der Joker schon benutzt wurde
		repeat = 1;				// Variable für die Wahl des Jokers
		jokerft = 1;            // Variable Zur Anzeige der Frage und Antworten bei Fehleingabe, wenn ein weiterer joker in einer Runde benutz werden soll
		jp1 = jokcat[3];		// Falls Joker gewaehlt wurde, Anzeige des Jokers auf einen leeren Feldplatz schieben
		jokcap1 = 4;			// Setzen der Ueberpruefungsvariablen auf einen Fehlwert
	}
	else if (jok == jokcap2) {
		repeat = 2;
		jokerft = 2;
		jp2 = jokcat[3];
		jokcap2 = 4;
	}
	else if (jok == jokcap3) {
		repeat = 3;
		jokerft = 3;
		jp3 = jokcat[3];
		jokcap3 = 4;
	}
	else {
		repeat = 0;				// Fehlwert, falls Eingabe nicht richtig ist
	}
	return repeat;				// Ausgabe der gewaehlten Jokervariablen (50/50, Telefon, Publikum)
}

void frage(int i, int k) {				// Ausgabe der Frage und Antworten
	Sleep(naptime);
	int j;
	j = gewinn[i];
	printf("\n  Frage %d : %d Euro", i + 1, j);		// Ausgabe der aktuellen Runde und dem dazugehoerigen Gewinn
	if (repeat == 1 || repeat == 0 && jokerft == 1) {					// Wird mit angegeben, wenn der 50/50 Joker gewaehlt wurde
		printf("\t-  50/50 Joker\n\n");
	}
	else if (repeat == 2 || repeat == 0 && jokerft == 2) {				// Wird mit angegeben, wenn der Telefonjoker gewaehlt wurde
		printf("\t-  Telefonjoker\n\n");
	}
	else if (repeat == 3 || repeat == 0 && jokerft == 3) {				// Wird mit angegeben, wenn der Publikumsjoker gewaehlt wurde
		printf("\t-  Publikumsjoker\n\n");
		if (helptpjok != 1) {
			jokerpubtxt();				// Gibt den Text fuer den Publikumsjoker 1-mal aus
			helptpjok = 1;
		}
	}
	else {
		printf("\n\n");
	}
	trenner();
	Sleep(naptime);
	printf("  %s ?\n\n\n", que[i].fn[k].frage); // Ausgabe der Frage
	if (repeat == 1 || repeat == 0 && jokerft == 1) { // Ausgabe der Frage und der Antworten, falls der 50/50 Joker gewaehlt wurde
		joker50(i, k, fz);
	}
	else if (repeat == 2 || repeat == 0 && jokerft == 2) { // Ausgabe der Frage und der Antworten, falls der Telefonjoker gewaehlt wurde
		jokertel(i, k, jokt);
		if (helpttjok != 1) { // Gibt den Text fuer den Telefonjoker 1-mal aus
			jokerteltxt();
			helpttjok = 1;
		}
	}
	else if (repeat == 3|| repeat == 0 && jokerft == 3) { // Ausgabe der Frage und der Antworten, falls der Publikumsjoker gewaehlt wurde
		jokerpub(i, k, p1, p2, p3, p4);
	}
	else { // Ausgabe wenn kein Joker gewaehlt wurde
		printf("\tA: %s\n\n\tB: %s\n\n\tC: %s\n\n\tD: %s\n\n", que[i].fn[k].antw1, que[i].fn[k].antw2, que[i].fn[k].antw3, que[i].fn[k].antw4);
	}
}

int k_antw(int i, int k) {			// Ueberpruefung welche Antwort die Korrekte ist
	if (strcmp(que[i].fn[k].antw1, que[i].fn[k].antwk) == 0) {
		korrekt = 1;				// Übergibt die korrekten Antwortnummer
	}
	else if (strcmp(que[i].fn[k].antw2, que[i].fn[k].antwk) == 0) {
		korrekt = 2;
	}
	else if (strcmp(que[i].fn[k].antw3, que[i].fn[k].antwk) == 0) {
		korrekt = 3;
	}
	else if (strcmp(que[i].fn[k].antw4, que[i].fn[k].antwk) == 0) {
		korrekt = 4;
	}
	return korrekt;					// Ausgabe der korrekten Antwort Nummer
}

int antworten(int i) {
	if (jokcount != 0) {				// Falls noch Joker Uebrig sind -----
		trenner();
		printf("\n  Um einen Joker einzusetzen tippen sie (j) ein\n\n"); // ---- Hinweis auf die Taste für den Joker
	}
	trenner();
	printf("  Ihre Antwort: ");
	//scanf_s("%c", &antweingabe);		// Eingabe des Antwortbuchstabens
	//getchar();
	antweingabe = _getch();
	if (isupper(antweingabe)) antweingabe = tolower(antweingabe);
	if (antweingabe == 97 && korrekt == 1 || antweingabe == 98 && korrekt == 2 || antweingabe == 99 && korrekt == 3 || antweingabe == 100 && korrekt == 4) {
		antwort = 1;				// Ueberpruefung ob die Eingabe der korrekten Antwort entspricht
	}
	else if (antweingabe == 97 && korrekt != 1 || antweingabe == 98 && korrekt != 2 || antweingabe == 99 && korrekt != 3 || antweingabe == 100 && korrekt != 4) {
		if (jokeruse != 0) {			// Ueberpruefung ob die Eingabe einer falschen Antwort entspricht
			if (jokeruse == 1) {
				antwort = 0;
			}
			else if (jokeruse == 2) {
				antwort = 0;			// Falls diese Antwortmoeglichkeit durch den 50/50 Joker nihct mehr moeglich ist, wird die Variable auf einen Fehlwert gesetzt 
			}
			if (jokeruse == 2) {
				antwort = 0;
			}
			else if (jokeruse == 3) {
				antwort = 0;
			}
			else if (jokeruse == 4) {
				antwort = 0;
			}
			else {
				antwort = 3;
			}
		}
		else {
			antwort = 0;
		}

	}
	else if (antweingabe == 106) {		// Ueberpruefung, ob ein Joker benutzt werden soll
		if (jokcount != 0) {			// Ueberpruefung, ob noch Joker uebrig sind
			antwort = 2;
		}
		else {
			antwort = 3;				// Falls kein joker mehr uebrig ist, wird die Variable auf einen Fehlwert gesetzt. 
		}
	}
	else {
		antwort = 3;					// Falls eine Taste gedrückt wurde, die nicht belegt ist, wird die Variable auf einen Fehlwert gesetzt
	}
	return antwort;							// Ausgabe der Antwortvariablen (Richtig, Falsch, Joker, Fehleingabe)
}

void einloggen() {			// Anzeige der gewählten Antwort
	if (antweingabe == 97) {
		printf("\tSie haben 'A' eingeloggt.\n\n");
	}
	else if (antweingabe == 98) {
		printf("\tSie haben 'B' eingeloggt.\n\n");
	}
	else if (antweingabe == 99) {
		printf("\tSie haben 'C' eingeloggt.\n\n");
	}
	else if (antweingabe == 100) {
		printf("\tSie haben 'D' eingeloggt.\n\n");
	}
	Sleep(1000);
	if (antweingabe == 97 && korrekt == 1) {			// Anzeigen ob Antwort korrekt ist oder falsch
		printf("\t\t\tIhre Antwort ist korrekt.\n");
	}
	else if (antweingabe == 98 && korrekt == 2) {
		printf("\t\t\tIhre Antwort ist korrekt.\n");
	}
	else if (antweingabe == 99 && korrekt == 3) {
		printf("\t\t\tIhre Antwort ist korrekt.\n");
	}
	else if (antweingabe == 100 && korrekt == 4) {
		printf("\t\t\tIhre Antwort ist korrekt.\n");
	}
	else {
		printf("\t\t\tIhre Antwort ist leider nicht korrekt");
	}
	Sleep(1200);
}

void verloren(int i) {											// Anzeige falls das Spiel verloren wurde
	system("cls");
	if (gewinn[i] > 300 && gewinn[i] < 16000) {			// Anzeige falls die erste Sicherheitsstufe erreicht wurde
		titel();
		printf("\n\n  Sie haben leider verloren, aber nehmen trotzdem %d Euro mit nach Hause.", gewinn[4]);
	}
	else if (gewinn[i] > 8000) {							// Anzeige falls die zweite Sicherheitsstufe erreicht wurde
		titel();
		printf("\n\n  Sie haben leider verloren, aber nehmen trotzdem %d Euro mit nach Hause.", gewinn[9]);
	}
	else {														// Anzeige falls keine Sicherheitsstufe erreicht wurde
		titel();
		printf("\n\n\t\t\tSie haben leider verloren.");
	}
	printf("\n\n\n\n\n  Weiter mit beliebiger Taste.");
	if (_kbhit()) {
		Sleep(2000);
	}
	_getch();
}

int zbild(int i) {							// Anzeige zwischen den Runden
	int hi;
	char entscheideingabe;
	titel();
	for (hi = 0; hi < 15; hi++) {			// Anzeige der Gewinnstufen
		if (hi == i) {
			printf("\t\t\t\t>> %d <<\t\n\n", gewinn[hi]);		// Anzeige der aktuellen Stufe
		}
		else if (hi == 4 || hi == 9) {							// Anzeige der Sicherheitsstufen
			printf("\t\t\t\t  -%d-\t\n\n", gewinn[hi]);
		}
		else {
			printf("\t\t\t\t   %d\t\n\n", gewinn[hi]);			// Anzeige aller anderen Stufen
		}
	}
	trenner();
	if (i > 8) {
		printf("  Falls sie die n\x84 \bchste Runde verlieren, haben sie trotzdem %d Euro gewonnen.\n\n", gewinn[9]);	// Hinweis ob man schon die zweite Gewinnstufe erreicht hat
	}
	else if (i < 9 && i > 3) {
		printf("  Falls sie die n\x84 \bchste Runde verlieren, haben sie trotzdem %d Euro gewonnen.\n\n", gewinn[4]);	// Hinweis ob man schon die erste Gewinnstufe erreicht hat
	}
	else {
		printf("  Falls sie die n\x84 \bchste Runde verlieren, haben sie leider nichts gewonnen.\n\n");					// Hinweis, dass man noch keine Gewinnstufe erreicht hat
	}
	trenner();
	printf("  Sie haben noch %d Joker\n\n", jokcount);			// Anzeige der Anzhal der verbleibenden Joker
	printf("  %s   %s   %s\n\n", jp1, jp2, jp3);				// Anzeige welche Joker uebrig sind
	trenner();
	printf("  Wollen sie weiterspielen?\n\n");					// Moeglichkeit freiwillig auszusteigen
	printf("  Ja (Enter)               Nein (Esc)\n\n");
	trenner();
	if (_kbhit()) {
		Sleep(2000);
		entscheideingabe = _getch();						// Eingabe der Entscheidung
	}
	else {
		entscheideingabe = _getch();
	}
	if (entscheideingabe == 13) {						// Ueberpruefung der Entscheidungseingabe
		entscheid = 0;
	}
	else if (entscheideingabe == 27) {
		entscheid = 1;
	}
	else {
		entscheid = 2;
	}
	system("cls");
	return entscheid;									// Ausgabe der Entscheidung( Ende, Weiter, Fehleingabe)
}

void ausstieg(int i) {				// Anzeige falls das Spiel selbststaendig beendet wurde
	system("cls");
	titel();
	printf("\n\n\tSie haben das Spiel freiwillig verlassen und %d Euro gewonnen.", gewinn[i]);			// Anzeige des Gewinns
	printf("\n\n\n\n\n  Weiter mit beliebiger Taste.");
	_getch();
}

int jokertree(int i, int k) {			// Anzeige falls ein Joker gwaehlt wurde
	for (;;) {							// Schleife damit die Breaks in der Funktion stehen koennen
		do {
			system("cls");
			titel();
			frage(i, k);				// Anzeige der Frage und der Antworten
			if (antwort == 2) {			// Falls ein Joker gewählt wurde ---
				joker();				// --- Anzeige für Jokerauswahl
			}
			else {						// Falls kein Joker gewählt wurde ---
				antworten(i);			// Anzeige der Antwortauswahl
			}
			//system("cls");
		} while (repeat != 1 && repeat != 2 && repeat != 3);	// Solange wiederholen bis eine gueltige Taste gedrueckt wurde
		jokcount--;					// Joker runterzaehlen. Start mit 3 Ende mit 0
			do {
				if (repeat == 1) {
					Sleep(500);
				}
				system("cls");
				titel();
				frage(i, k);
				antworten(i);
				if (antwort != 2 && antwort != 3) {		// Falls kein weiterer Joker weiterer Joker benutzt werden soll
					einloggen();						// Anzeige der ausgewählten Antwort
				}
			} while (antwort != 0 && antwort != 1 && antwort != 2);		// Solange wiederholen bis eine gueltige Taste gedrueckt wurde
			if (antwort == 0) {				// Falls die Antwort falsch war
				verloren(i);					// Anzeige fuer verlorenes Spiel anzeigen
				breaker = 1;				// Variable um den break in main() auszuloesen 
				break;						// Beenden der Schleife in der Funktion
			}
			else if (antwort == 2) {			// Falls noch ein Joker gewaehlt wurde
				breaker = 2;				// Variable falls noch ein Joker gewählt wurde
				break;						// Beenden der Schleife in der Funktion
			}
			else if (antwort == 1) {	// Falls die Antwort korrekt wahr
				if (i == 14) {			// Anzeige fuer gewonnenes Spiel, falls die letzte Runde geschafft wurde
					system("cls");
					titel();
					printf("\n\n\t\t\tGl\x81 \bckwunsch,\n\n\t\tSie haben alle Fragen richtig beantwortet\n\n\t\tund gehen mit %d Euro nach Hause.", gewinn[i]);
					printf("\n\n\n\n\n  Weiter mit beliebiger Taste.");
					_getch();
					break;
				}
				else {							// Falls die letzte Runde noch nicht erreicht wurde
					system("cls");
					do {
						zbild(i);				// Anzeige zwischen den Runden
					} while (entscheid > 1);	// Solange ausfuehren bis die Eingabe korrekt ist
					if (entscheid == 1) {		// Falls freiwilliger Ausstieg
						ausstieg(i);			// Anzeige des Gewinns
						breaker = 1;
						break;
					}
					else if (entscheid == 0) {	// Falls die naechste Runde gespielt wird
						system("cls");
						break;
					}
				}
			}
	}
	return breaker;				// Ausgabe für main() (break, Joker)
}

int ende() {						// Menue am Ende des Spiels
	char endeingabe;
	titel();
	printf("\n\n\t\t\tWollen sie nocheinmal spielen?\n\n");
	printf("\n\n\t\t\tJa (Enter)\n\n\t\t\tNein (Esc)\n\n\n");
	trenner();
	//scanf_s("%c", &endeingabe);		
	//getchar();
	endeingabe = _getch();		// Eingabe der Wahl, nochmal spielen oder beenden
	system("cls");
	if (endeingabe == 13) {		//Ueberpruefung der Wahlentscheidung
		end = 0;
	}
	else if (endeingabe == 27) {
		end = 1;
		printf("\n\n\n\n\n\n\n\t\t\t\tDanke f\x81rs spielen! \n\n\n");
		Sleep(2000);
	}
	else {
		end = 2;
	}
	return end;						// Ausgabe der Wahlentscheidung( nochmal, beenden, Fehleingabe)
}


int main() {									// Hauptablauf
	system("color 80");
	do {
		do {
			willkommen();
		} while (end != 0 && end != 1);
		if (end == 1) {
			break;
		}
		reset();							// Zurücksetzen der Variablen
		srand(time(NULL));
		for (i = 0; i < 15; i++) {			// Start der Schleife fuer 15 Runden
			zufallszahl(i);					// Zufallszahlen initiieren
			k_antw(i, k);				// Korrekte Antworten initiieren
			do {
				system("cls");
				titel();					// Anzeige des Titels
				frage(i, k);				// Anzeige der Frage und der Antworten
				antworten(i);				// Anzeige der Antworteingabe
				if (antwort != 2 && antwort != 3) {
					einloggen();
				}
			} while (antwort != 0 && antwort != 1 && antwort != 2); // Solange wiederholen bis die Eingabe gueltig ist
			if (antwort == 0) {				// Falls Antwort falsch ist
				verloren(i);				// Anzeige fuer verlorenes Spiel anzeigen
				break;						// Schleife verlassen
			}
			else if (antwort == 2 && jokcount != 0) { // Falls Joker gewahlt werden soll
				jokertree(i, k);						  // Anzeige falls Joker gewaehlt wurde
				if (breaker == 1) {					  // den break aus der Funktion uebernehmen
					breaker = 0;					  // Variable für den break zuruecksetzen	
					break;							  // Schleife verlassen
				}
				else if (breaker == 2) {			  // Falls in der selben Runde des ersten Jokers noch einer gewählt wurde
					breaker = 0;
					jokertree(i, k);
					if (breaker == 1) {
						breaker = 0;
						break;
					}
					else if (breaker == 2) {		  // Falls in der selben Runde des ersten und zweiten Jokers noch einer gewählt wurde
						breaker = 0;
						jokertree(i, k);
						if (breaker == 1) {
							breaker = 0;
							break;
						}
					}
				}
				repeat = 0;				// Zurücksetzen der Variablen für die Joker fuer die naechste Runde
				jokerusek = 0;
				jokeruse = 0;
				jokerft = 0;
			}
			else if (antwort == 1) {	// Falls die Antwort korrekt wahr
				if (i == 14) {			// Anzeige fuer gewonnenes Spiel, falls die letzte Runde geschafft wurde
					system("cls");
					titel();
					printf("\n\n\t\t\tGl\x81 \bckwunsch,\n\n\t\tSie haben alle Fragen richtig beantwortet\n\n\t\tund gehen mit %d Euro nach Hause.", gewinn[i]);
					printf("\n\n\n\n\n  Weiter mit beliebiger Taste.");
					_getch();
				}
				else {							// Falls die letzte Runde noch nicht erreicht wurde
					system("cls");
					do {
						zbild(i);				// Anzeige zwischen den Runden
					} while (entscheid > 1);	// Solange ausfuehren bis die Eingabe korrekt ist
					if (entscheid == 1) {		// Falls freiwilliger Ausstieg
						ausstieg(i);				// Anzeige des Gewinns
						break;
					}
					else if (entscheid == 0) {	// Falls die naechste Runde gespielt wird
						system("cls");
					}
				}
			}
		}
		do {
			system("cls");
			ende();							// Anzeige um das Spiel neuzustarten
		} while (end != 0 && end != 1);		// Solange ausfuehren bis die Eingabe korrekt ist
	} while (end == 0);						// Ueberpruefung ob das Spiel noch einmal ausgefuehrt werden soll
	return 0;
}