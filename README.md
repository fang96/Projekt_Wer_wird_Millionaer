Das Projekt hat den Namen "Wer wird Millionär" und wird von Baron, Sebastian und Angirillo, Fabio durchgeführt.

Wer wird Millionär ist das klassische Quizspiel, indem man um Geldbeträge spielt. Beantwortet man eine Frage, so sichert man sich den Betrag 
und es geht zur nächsten Frage. Der Geldbetrag steigt nach jeder Frage, bis man am Ende die Million erreicht. Nach jeder fünften Frage erreicht 
man eine Sicherheitsstufe. Wenn man eine Frage falsch beantwortet, dann ist das Spiel zu Ende gegangen und man gewinnt den Betrag der zuletzt erreichten
Sicherheitsstufe. Das Spiel endet außerdem, wenn man die Millionenfrage richtig beantwortet oder freiwillig aussteigt. Letzteres führt dazu, dass
der Spieler den zuletzt erreichten Geldbetrag gewinnt. Außerdem hat man noch 3 Joker zur Verfügung, wovon nur eines in Code realisierbar ist.

Unser Programm wird die Funktion haben, dass man bei der Auswahl einer Frage eine zufällige Zahl generiert und jede Frage einer Zahl 
im Generierungsbereich angehört. Nach jeder Frage wird die Zahl jener Frage auf 0 gesetzt, damit sie nicht wieder rankommen kann. 
Die Fragen werden in Funktionen geschrieben. In den Funktionen stehen die Antwortmöglichkeiten, die Frage und die richtige Antwort. 
Eine Variable wird als Argument in jeder Funktion der Fragen hinzugefügt, die entweder 0 oder 1 ist je nachdem die Frage falsch oder richtig
beantwortet wurde. 

Die wahrscheinlich größte technische Schwierigkeit werden die Funktionen der Fragen sein und ihre Abhängigkeit zum Rest des Programms.
Argumente könnte man in Strukturen zusammenfassen.