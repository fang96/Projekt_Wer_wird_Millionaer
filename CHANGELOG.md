########################################

2015-12-20 

- "CHANGELOG.md" wurde erstellt

- "CONTRIBUTING.md" wurde erstellt
           
- "README.md" wurde dem Projekt hinzugefügt

2016-1-4

- Konzeptablauf aufgeschrieben

- Deklaration der Variabelen

- Zufallsgenerator für Runden und Variabelen für Geldbeträge erstellt -> Funktionen und Arrays dafür angefertigt

2016-1-5

- Fragen rausgesucht und notiert

- Fragen in Code umgewandelt (neue Datei angefertigt und Strukturen gebildet).

- 90 Fragen. 15 werden ausgesucht, wovon die "Schwierigkeit" stetig steigt -> 1. Frage eine Auswahl von 6 Zahlen, sowie 2,3 usw. 


2016-1-6

- Grundgerüst des Programms fertig gemacht (Programm läuft einigermaßen).

- Dazu gehören: Funktionen für die Ausgestaltung der Konsole, 2 von 3 Joker erstellt, Funktion für den Ablauf, Aus und Eingabe, Game Over Screen etc.

2016-1-7

- Verfeinerung des Programms

- Am letzten Joker (Publikumsjoker) arbeiten.

2016-1-8

- Verfeinerungen am Code

- Letzen Joker fertig gemacht

- Code kommentiert

2016-1-9

- Projekt beendet und finale Version hochgeladen.


